import React, { useState } from 'react'
import styles from './Content.module.css'

import { MessageBus } from '@podium/browser'

function Content() {
  const messageBus = new MessageBus()
  const [items, setItems] = useState([])

  messageBus.subscribe('internalchannel', 'newTodo', event => {
    if (event.payload.message.join('') !== items.join('')) {
      setItems(event.payload.message)
    }
  })

  return (
    <div className={styles.wrapper}>
      <span className={styles.header}>Micro Frontend Content</span>
      <ul className={styles.items}>
        {items.map(item => (
          <li className={styles.item}>{item}</li>
        ))}
      </ul>
    </div>
  )
}

export default Content
