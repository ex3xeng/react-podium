import React from 'react'
import ReactDOM from 'react-dom'

import './assets/main.css'
import Content from './components/Content'

ReactDOM.render(
  <React.StrictMode>
    <Content />
  </React.StrictMode>,
  document.getElementById('react-content')
)
