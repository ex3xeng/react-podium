const express = require('express')
const Podlet = require('@podium/podlet')
const fs = require('fs')

const app = express()

const podlet = new Podlet({
  name: 'contentFrontend',
  version: '0.0.1',
  pathname: '/',
  manifest: '/manifest.json',
  development: true
})

const rawdata = fs.readFileSync('build/asset-manifest.json')
const assets = JSON.parse(rawdata)

assets.entrypoints.forEach(element => {
  if (element.indexOf('.css') !== -1) {
    podlet.css({ value: 'http://localhost:7101/' + element })
  }

  if (element.indexOf('.js') !== -1) {
    podlet.js({ value: 'http://localhost:7101/' + element, defer: true })
  }
})

app.use(podlet.middleware())
app.use('/static', express.static('build/static'))

app.get(podlet.content(), (_, res) => {
  res.status(200).podiumSend('<div id="react-content"></div>')
})

app.get(podlet.manifest(), (_, res) => {
  res.status(200).send(podlet)
})

app.listen(7101)
