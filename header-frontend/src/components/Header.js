import React, { useState } from 'react'
import styles from './Header.module.css'

import logo from '../assets/logo.svg'

import { MessageBus } from '@podium/browser'

function Header() {
  const messageBus = new MessageBus()

  const [todo, setTodo] = useState('')
  const [list, setList] = useState([])

  function handleChange(e) {
    setTodo(e.target.value)
  }

  const handleInput = () => {
    const input = [...list, todo]

    messageBus.publish('internalchannel', 'newTodo', {
      message: input,
      from: 'header-frontend'
    })

    setList(input)
    setTodo('')
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <img
          className={styles.logo}
          src={window.location.origin.replace('7000', '7100') + logo}
          alt="logo"
        />
        <span className={styles.text}>Micro Frontend Header</span>
      </div>
      <div className={styles.content}>
        <input
          className={styles.input}
          type="text"
          value={todo}
          onChange={handleChange}
          placeholder="Название"
        />
        <button className={styles.button} onClick={handleInput}>
          Добавить
        </button>
      </div>
    </div>
  )
}

export default Header
