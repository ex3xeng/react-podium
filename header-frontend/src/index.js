import React from 'react'
import ReactDOM from 'react-dom'

import './assets/main.css'
import Header from './components/Header'

ReactDOM.render(
  <React.StrictMode>
    <Header />
  </React.StrictMode>,
  document.getElementById('react-header')
)
