const express = require('express')
const app = express()

const Layout = require('@podium/layout')

const layout = new Layout({ name: 'homeLayout', pathname: '/' })

const headerFrontend = layout.client.register({
  name: 'headerFrontend',
  uri: 'http://localhost:7100/manifest.json'
})

const contentFrontend = layout.client.register({
  name: 'contentFrontend',
  uri: 'http://localhost:7101/manifest.json'
})

app.use(layout.middleware())

app.get('/', async (_, res) => {
  const incoming = res.locals.podium

  const content = await Promise.all([
    headerFrontend.fetch(incoming),
    contentFrontend.fetch(incoming)
  ])

  incoming.podlets = content
  incoming.view.title = 'Home Page'

  res.podiumSend(`<div>
    ${content[0]}
    ${content[1]}
  </div>
  `)
})

app.listen(7000)
